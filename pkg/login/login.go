package login

import (
	"bytes"
	"encoding/json"
	"log"

	"net/http"

	"git.flightfactor.aero/x-updater-client/internal/models"
)

func Login(name string, key string) (*models.Product, error) {

	product := &models.Product{}
	posturl := "https://cloudflare-d1-proxy.remotejobtest8250.workers.dev/v1/consumers/login"

	body := []byte(`{"name":"last@psu.edu","key":"283133a8-1a76-4b51-9a40-d2f4299dae98"}`)

	r, err := http.NewRequest("POST", posturl, bytes.NewBuffer(body))
	if err != nil {

		return product, err
	}

	r.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {

		return product, err

	}

	defer res.Body.Close()

	derr := json.NewDecoder(res.Body).Decode(product)
	if derr != nil {
		return product, derr

	}

	log.Println("prod",product)

	return product, nil

}
