package models

import "github.com/volatiletech/null/v8"

type ConsumerLogin struct {
	Name string `json:"name" yaml:"name" example:"last@psu.edu"`
	Key  string `json:"key" toml:"key" yaml:"key" example:"3d638d80-d3d7-4149-82ba-ab0d3aec3d2b"`
}

type Product struct {
	ID int `boil:"id" json:"id" toml:"id" yaml:"id"`
	// Id of the project a product belongs to.
	ProjectID int `boil:"project_id" json:"project_id" toml:"project_id" yaml:"project_id"`
	// This is 16 bytes of uuid-V7. Represents public unique id of a product. Can be used in various sceneries to identifies the product.
	PublicID string `boil:"public_id" json:"public_id" toml:"public_id" yaml:"public_id"`
	// It is the id from the old database and the old xupdater version and used for backward compatibility. Some day it should be dropped (when all consume clients are able to work with new variant).
	PublicIDLegacy null.String `boil:"public_id_legacy" json:"public_id_legacy,omitempty" toml:"public_id_legacy" yaml:"public_id_legacy,omitempty"`
	// Not 0 if a product has public beta access.
	PublicBeta null.Uint8 `boil:"public_beta" json:"public_beta,omitempty" toml:"public_beta" yaml:"public_beta,omitempty"`
	// Directory name where a product is stored on the server. Relative to the distributor's one.
	DirName string `boil:"dir_name" json:"dir_name" toml:"dir_name" yaml:"dir_name"`
	// A product name that is used inside the system.
	LocalName string `boil:"local_name" json:"local_name" toml:"local_name" yaml:"local_name"`
	// A product name that is read from a store database. I am not sure if it should be unique, so it is normal if we turn uniqueness off in future.
	StoreName null.String `boil:"store_name" json:"store_name,omitempty" toml:"store_name" yaml:"store_name,omitempty"`
	// Id of a store product this product is associated to.
	StoreProductID null.Uint `boil:"store_product_id" json:"store_product_id,omitempty" toml:"store_product_id" yaml:"store_product_id,omitempty"`
	// Block message, null - means the product is not blocked otherwise it is blocked with the message. Is used to manage product distribution: start/pause/stop
	BlockMSG null.String `boil:"block_msg" json:"block_msg,omitempty" toml:"block_msg" yaml:"block_msg,omitempty"`
	// Any text note.
	Notes    null.String `boil:"notes" json:"notes,omitempty" toml:"notes" yaml:"notes,omitempty"`
	Created  string      `boil:"created" json:"created" toml:"created" yaml:"created"`
	Modified string      `boil:"modified" json:"modified" toml:"modified" yaml:"modified"`
}
