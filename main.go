package main

import (
	"fmt"
	"image/color"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"git.flightfactor.aero/x-updater-client/pkg/download"
	"git.flightfactor.aero/x-updater-client/pkg/login"

	"fyne.io/fyne/v2/container"

	"fyne.io/fyne/v2/app"

	"fyne.io/fyne/v2/widget"
)

var a fyne.App
var w fyne.Window
var c *fyne.Container
var dirbutton *widget.Button

// var str binding.String

func main() {

	// New app
	a = app.New()
	// // New Window & title
	w = a.NewWindow("App")

	items := []*widget.FormItem{}

	entryName := widget.NewEntry()
	fItemName := widget.NewFormItem("name", entryName)
	items = append(items, fItemName)

	entryKey := widget.NewEntry()
	fItemKey := widget.NewFormItem("key", entryKey)
	items = append(items, fItemKey)

	form := &widget.Form{
		Items: items,
		OnSubmit: func() { // optional, handle form submission
			log.Println("Form submitted:", entryName.Text)
			// log.Println("multiline:", textArea.Text)

			product, err := login.Login("username.Text", "password.Text")
			if err != nil {
				log.Fatal(err)
			}

			c.RemoveAll()

			text1 := canvas.NewText("Product:", color.White)
			text2 := canvas.NewText(product.LocalName, color.NRGBA{R: 0, G: 180, B: 0, A: 255})
			text3 := canvas.NewText("Remote Dir", color.White)
			text4 := canvas.NewText(product.DirName, color.NRGBA{R: 0, G: 180, B: 0, A: 255})
			dirbutton = widget.NewButton("Select Local Dir", func() {

				dialog.ShowFolderOpen(func(read fyne.ListableURI, err error) {

					if read == nil {

						log.Println("Dir Not selected ")

					} else {

						fmt.Println("User chose:", read.Path(), err)
						pathtodownload :=read.Path()
						c.RemoveAll()

						text1 := canvas.NewText("Selected Dir:", color.White)
						text2 := canvas.NewText(read.Path(), color.NRGBA{R: 0, G: 180, B: 0, A: 255})
						dirbutton = widget.NewButton("Try download ", func() {

							log.Println("Try download")

							err :=download.DownloadFile(pathtodownload+"/finalprod.bash", "https://cloudflare-d1-proxy.remotejobtest8250.workers.dev/download")
							if err != nil {

								log.Fatalln(err)
							}

						})

						cc := container.New(layout.NewGridLayout(2), text1, text2, dirbutton)

						cc.Move(fyne.NewPos(40, 550))

						c.Add(cc)

					}
				}, w)

			})
			cc := container.New(layout.NewGridLayout(2), text1, text2, text3, text4, dirbutton)

			cc.Move(fyne.NewPos(40, 550))
			c.Add(cc)

			// c.Objects = c.Objects[0:]

			// c.Refresh()

		},
	}

	form.Resize(fyne.NewSize(250, 30))
	form.Move(fyne.NewPos(40, 150))

	c = container.NewWithoutLayout(
		form,
	)

	w.SetContent(

		c,
	)

	w.ShowAndRun()
}

func closeCallback() {

	log.Println("Check")

	log.Println(len(c.Objects))
	// w.Content().

}
